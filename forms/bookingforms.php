<section id="projects" class="projects-section bg-light">
      <div class="container">

        <!-- Featured Project Row -->
        <form method = "post" action="booking.php">
        <div class="row align-items-center no-gutters mb-4 mb-lg-5">
          
          <div class="featured-text col-xl-7 col-lg-7">
            <h4 class="text-uppercase m-0">Come on, let's create memories...</h4><br>
            <h6>Step 1: Enter your Email <input type="text" name="email"></h6> 
            <h6>Step 2: Choose your Package</h6>
                <table>
                  
                  <tr>
                    <td>  Events</td>
                    <td>  Package 1</td>
                    <td colspan="4">  Package 2</td>
                    <td colspan="4">  Package 3</td>
                  </tr>

                  <tr>
                    <th>  Studio Rentals</th>
                    <td><input type="radio" name="package" value="SrPack1">  500php per hour <br> How many hours: <input type="text" name="hours"></td><br>
                    <td colspan="4"><input type="radio" name="package"value="SrPack2">  3,000php <br> <input type="hidden" id="priceID" name="priceID" value="3,000php"> <input type="hidden" id="priceID2" name="priceID2" value="3000"></td>
                    <td colspan="4"></td>
                  </tr><br>

                  <tr>
                    <th>  Wedding</th>
                    <td><input type="radio" name="package" value ="WedPack1"> 20,000php <br> <input type="hidden" name="priceID" value="20,000php"> <input type="hidden" id="priceID2" name="priceID2" value="20000"></td>
                    <td colspan="4"><input type="radio" name="package" value="WedPack2">  25,000php <br> <input type="hidden" id="priceID" name="priceID" value="25,000php"> <input type="hidden" id="priceID2" name="priceID2" value="25000"></td>
                    <td colspan="4"><input type="radio" name="package" value="WedPack3">  70,000php <br> <input type="hidden" id="priceID" name="priceID" value="70,000php"> <input type="hidden" id="priceID2" name="priceID2" value="70000"> </td>
                  </tr><br>

                  <tr>
                    <th>  Photobooth</th>
                    <td><input type="radio" name="package" value="PbPack1"> 3,500php <br> <input type="hidden" id="priceID" name="priceID" value="3,500php"> <input type="hidden" id="priceID2" name="priceID2" value="3500"></td>
                    <td colspan="4"></td>
                    <td colspan="4"></td>
                  </tr><br>

                  <tr>
                    <th>  Birthday/Christening</th>
                    <td><input type="radio" name="package" value="BdayPack1"> 8,000php <br> <input type="hidden" id="priceID" name="priceID" value="8,000php"> <input type="hidden" id="priceID2" name="priceID2" value="8000"> </td>
                    <td colspan="4"><input type="radio" name="package" value="BdayPack2"> 15,000php <br> <input type="hidden" id="priceID" name="priceID" value="15,000php"> <input type="hidden" id="priceID2" name="priceID2" value="15000"></td>
                    <td colspan="4"><input type="radio" name="package" value="BdayPack3"> 20,000php <br> <input type="hidden" id="priceID" name="priceID" value="20,000php"> <input type="hidden" id="priceID2" name="priceID2" value="20000"></td>
                  </tr><br>

                  <tr>
                    <th>  Makeup Stylist</th>
                    <td><input type="radio" name="package" value="StylePack1">  1,500php per hour <br> How many hours: <input type="text" name="hours"> <input type="hidden" id="priceID" name="priceID" value="1,500php"> <input type="hidden" id="priceID2" name="priceID2" value="1500"> </td>
                    <td colspan="4"></td>
                    <td colspan="4"></td>
                  </tr><br>

                  <tr>
                    <th>  Debut</th>
                    <td><input type="radio" name="package" value="DebPack1">  22,000php <br> <input type="hidden" id="priceID" name="priceID" value="22,000php"> <input type="hidden" id="priceID2" name="priceID2" value="22000"> </td>
                    <td colspan="4"><input type="radio" name="package" value="DebPack2">  30,000php <br> <input type="hidden" id="priceID" name="priceID" value="30,000php"> <input type="hidden" id="priceID2" name="priceID2" value="30000"></td>
                    <td colspan="4"><input type="radio" name="package" value="DebPack3">  35,000php <br> <input type="hidden" id="priceID" name="priceID" value="35,000php"> <input type="hidden" id="priceID2" name="priceID2" value="35000"></td>
                  </tr><br>

                  <tr>
                    <th>  Pre Nuptial</th>
                    <td><input type="radio" name="package" value="PreNup1"> 15,000php <br> <input type="hidden" id="priceID" name="priceID" value="15,000php"> <input type="hidden" id="priceID2" name="priceID2" value="15000"></td>
                    <td colspan="4"><input type="radio" name="package" value="PreNup2"> 15,000php <br> <input type="hidden" id="priceID" name="priceID" value="15,000php"> <input type="hidden" id="priceID2" name="priceID2" value="15000"></td>
                    <td colspan="4"></td>
                  </tr><br>

                </table><br>


                <h6>Step 3: Enter your date: <input type="date" name="dates"></h6>
                <h6>Step 4: Enter your time for the Event: <input type="time" name="taym"></h6>
                <p><input type= "submit" name = "submit_btn" class="btn btn-primary" value="Submit"></p>
          </div>
          
        </div>
      </section>