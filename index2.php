<?php include 'php/header4.php';
include ('phpconnections/connection.php');
include ('phpconnections/function.php');?>


 <header class="masthead">
      <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
          <h2 class="text-white-50 mx-auto mt-2 mb-5">welcome to</h2>
          <h1 class="mx-auto my-0 text-uppercase">Studio 4104</h1><br>
          <h2 class="text-white-50 mx-auto mt-2 mb-5"><?php
                if (loggedIn()) 
                {
                  echo "Hello!" . " " . $_SESSION['uemail'];
                } 
                else 
                  {
                    echo "Hello! Guest";
                  } 
                ?></h2>        

          <a href="booking.php" class="btn btn-primary js-scroll-trigger">Book Now</a>
          <a href="checkbooking(client).php" class="btn btn-primary js-scroll-trigger">Check Booking</a>
        </div>
      </div>
    </header>

  <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/grayscale.min.js"></script>

</body>
</html>