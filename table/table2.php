<div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Customer List</h2>
					</div>
                </div>
            </div>

            <br><br>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
						<th>Email</th>
                        <th>Password</th>
                        <th>Date Created</th>
                        <th>Already Registered?</th>
                    </tr>
                </thead>
                <tbody>

                	<?php 
                	$result = $conn->query("SELECT * FROM user");
                	while($data = mysqli_fetch_object($result)):
                		?>
                    <tr>
                        <td><?php echo $data->uid?></td>
                        <td><?php echo $data->Fname." ".$data->Mname." ".$data->Lname?></td>
                        <td><?php echo $data->address?></td>
						<td><?php echo $data->uemail?></td>
                        <td><?php echo $data->upass?></td>
                        <td><?php echo $data->rdate?></td>
                        <td><?php echo $data->vstatus?></td>
                    </tr>
                <?php 
            	endwhile;
                ?>
                </tbody>
            </table>
    </div>
	
	