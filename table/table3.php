<div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Check Booking</h2>
					</div>
                </div>
            </div>

            <br><br>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
						<th>Package</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Price</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>

                	<?php 
                	$result = $conn->query("SELECT * FROM book");
                	while($data = mysqli_fetch_object($result)):
                		?>
                    <tr>
                        <td><?php echo $data->id?></td>
                        <td><?php echo $data->email?></td>
						<td><?php echo $data->package?></td>
                        <td><?php echo $data->dates?></td>
                        <td><?php echo $data->taym?></td>
                        <td><?php echo $data->priceID?></td>
                        <td><?php echo $data->bstatus?></td>
                        <td>
                            <a href="#editEmployeeModal" class="edit" data-toggle="modal"><img src="img/edit.jpg"></a>
                            <a href="#deleteEmployeeModal" class="delete" data-toggle="modal"><img src="img/delete.jpg"></a>
                        </td>
                    </tr>
                <?php 
            	endwhile;
                ?>
                </tbody>
            </table>
    </div>
	<!-- Edit Modal HTML -->
	
	<div id="editEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Edit Status</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="Password" class="form-control" required>
						</div>			
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-info" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Delete Employee</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>Are you sure you want to delete these Records?</p>
						<p class="text-warning"><small>This action cannot be undone.</small></p>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" value="Delete">
					</div>
				</form>
			</div>
		</div>
	</div>