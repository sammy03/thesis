<!-- About Section--> 
    <section id="about" class="about-section text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h2 class="text-white mb-4">Welcome to Studio 4104!!!</h2><br>
            <p class="text-white-50">"Images of people, cities, and landscapes from the air tell a unique story about our personal space and how we relate to one another. I've always aimed to address the bigger picture and later trends. In many ways, what a photographer does is give others a chance to step back and look at their world and gain perspective on where we stand and what that means."<br><br> -Vincent Laforet </p>
            
          </div>
        </div>
      </div>
      <br><br><br>
    </section>

    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light">
      <div class="container">

        <!-- Featured Project Row -->
        <div class="row align-items-center no-gutters mb-4 mb-lg-5">
          <div class="col-xl-8 col-lg-7">
            <img class="img-fluid mb-3 mb-lg-0" src="img/bg1.jpg" alt="">
          </div>
          <div class="col-xl-4 col-lg-5">
            <div class="featured-text text-center text-lg-left">
              <h4>Photography is an art of observation. It has a little to do with the things you see and everything to do with the way you see them.</h4>
              <p class="text-black-50 mb-0"> - Elliot Erwitt</p><br>
              <a href="gallery.php" class="btn btn-primary js-scroll-trigger">View gallery</a>
            </div>
          </div>
        </div>

        <!-- Project One Row -->
        <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
          <div class="col-lg-6">
            <img class="img-fluid" src="img/bg2.jpg" alt="">
          </div>
          <div class="col-lg-6">
            <div class="bg-black text-center h-100 project">
              <div class="d-flex h-100">
                <div class="project-text w-100 my-auto text-center text-lg-left">
                  <h4 class="text-white">Debut</h4>
                  <p class="mb-0 text-white-50">The Debut is a traditional Filipino coming-of-age celebration which celebrates a young woman's 18th birthday, the age of maturity in the Philippines. Although also reaching legal maturity at 18, a Filipino man may mark his own debut on his 21st birthday, albeit with less formal clebrations or none at all.</p><br>
                </div>
              </div>
            </div>
          </div>
        </div>
  
        <!-- Project Two Row -->
        <div class="row justify-content-center no-gutters">
          <div class="col-lg-6">
            <img class="img-fluid" src="img/bg3.jpg" alt="">
          </div>
          <div class="col-lg-6 order-lg-first">
            <div class="bg-black text-center h-100 project">
              <div class="d-flex h-100">
                <div class="project-text w-100 my-auto text-center text-lg-right">
                  <h4 class="text-white">Birthday/Christening</h4>
                  <p class="mb-0 text-white-50">Your child's Christening/Baptism or his/her first birthday is one of the first milestones they experience in their life. It is a special event for the child's family from attending the church ceremony for the christening up to the reception to celebrate also the child's 1st birthday.</p><br>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Project Third Row -->
        <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
            <div class="col-lg-6">
              <img class="img-fluid" src="img/bg4.jpg" alt="">
            </div>
            <div class="col-lg-6">
              <div class="bg-black text-center h-100 project">
                <div class="d-flex h-100">
                  <div class="project-text w-100 my-auto text-center text-lg-left">
                    <h4 class="text-white">Wedding</h4>
                    <p class="mb-0 text-white-50">A Wedding is a ceremony where two people are united in marriage. Wedding traditions and customs vary greatly between cultures, ethnic groups, religions, countries, and social classes. Most wedding ceremonies involve an exchange of marriage vows by the couple and presentation of a gift (offerings, rings, symbolic item, flowers, money).</p><br>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          <!-- Project Fourth Row -->
          <div class="row justify-content-center no-gutters">
            <div class="col-lg-6">
              <img class="img-fluid" src="img/bg5.jpg" alt="">
            </div>
            <div class="col-lg-6 order-lg-first">
              <div class="bg-black text-center h-100 project">
                <div class="d-flex h-100">
                  <div class="project-text w-100 my-auto text-center text-lg-right">
                    <h4 class="text-white">Pre Nuptial</h4>
                    <p class="mb-0 text-white-50">A Pre Nuptial shoot is a wonderful way of commemorating your wedding. It is also a special way of imprinting your flair on photos that you share with your family and friends. These days, more couples arrange for a prenuptial shoot that is creative and unique and representative of their style and personalities. </p><br>
                  </div>
                </div>
              </div>
            </div>
          </div>

      </div>
    </section>