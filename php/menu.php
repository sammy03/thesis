<div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#page-top">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="services.php">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="gallery.php">Gallery</a>
            </li>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#signup">Contact Us</a>
            </li>

            
            <div class="dropdown">
                <li class="nav-link js-scroll-trigger">More</li>
                <div class="dropdown-content">
                <a href="register.php">Registration</a>
                <a href="login.php">Login</a>
                <a href="adminlogin.php">Admin</a>
                </div>
            </div>
            

            
          </ul>
        </div>