<?php include 'php/header2.php' ?>

<section id="projects" class="projects-section bg-light">
    <div class="container">

      <!-- Featured Project Row -->
   <!--   <div class="row align-items-center no-gutters mb-4 mb-lg-5"> -->
        <div class="col-md-12 mb-9 mb-md-0">
          <div class="gallery">
            <a href="img/pictures-large/51.jpg"> <img src="img/pictures-small/51-small.jpg"></a>
            <a href="img/pictures-large/52.jpg"> <img src="img/pictures-small/52-small.jpg"></a>
            <a href="img/pictures-large/53.jpg"> <img src="img/pictures-small/53-small.jpg"></a>
            <a href="img/pictures-large/54.jpg"> <img src="img/pictures-small/54-small.jpg"></a>
            <a href="img/pictures-large/55.jpg"> <img src="img/pictures-small/55-small.jpg"></a>
            <a href="img/pictures-large/56.jpg"> <img src="img/pictures-small/56-small.jpg"></a>
            <a href="img/pictures-large/57.jpg"> <img src="img/pictures-small/57-small.jpg"></a>
            <a href="img/pictures-large/58.jpg"> <img src="img/pictures-small/58-small.jpg"></a>
            <a href="img/pictures-large/59.jpg"> <img src="img/pictures-small/59-small.jpg"></a>
            <a href="img/pictures-large/60.jpg"> <img src="img/pictures-small/60-small.jpg"></a>
            <a href="img/pictures-large/61.jpg"> <img src="img/pictures-small/61-small.jpg"></a>
            <a href="img/pictures-large/62.jpg"> <img src="img/pictures-small/62-small.jpg"></a>
            <a href="img/pictures-large/63.jpg"> <img src="img/pictures-small/63-small.jpg"></a>
            <a href="img/pictures-large/64.jpg"> <img src="img/pictures-small/64-small.jpg"></a>
            <a href="img/pictures-large/65.jpg"> <img src="img/pictures-small/65-small.jpg"></a>
            <a href="img/pictures-large/66.jpg"> <img src="img/pictures-small/66-small.jpg"></a>
            <a href="img/pictures-large/67.jpg"> <img src="img/pictures-small/67-small.jpg"></a>
            <a href="img/pictures-large/68.jpg"> <img src="img/pictures-small/68-small.jpg"></a>
            <a href="img/pictures-large/69.jpg"> <img src="img/pictures-small/69-small.jpg"></a>
            <a href="img/pictures-large/70.jpg"> <img src="img/pictures-small/70-small.jpg"></a>
            <a href="img/pictures-large/71.jpg"> <img src="img/pictures-small/71-small.jpg"></a>
            <a href="img/pictures-large/72.jpg"> <img src="img/pictures-small/72-small.jpg"></a>
            <a href="img/pictures-large/73.jpg"> <img src="img/pictures-small/73-small.jpg"></a>
            <a href="img/pictures-large/74.jpg"> <img src="img/pictures-small/74-small.jpg"></a>
            <a href="img/pictures-large/75.jpg"> <img src="img/pictures-small/75-small.jpg"></a>
            <a href="img/pictures-large/76.jpg"> <img src="img/pictures-small/76-small.jpg"></a>
            <a href="img/pictures-large/77.jpg"> <img src="img/pictures-small/77-small.jpg"></a>
            <a href="img/pictures-large/78.jpg"> <img src="img/pictures-small/78-small.jpg"></a>
            <a href="img/pictures-large/79.jpg"> <img src="img/pictures-small/79-small.jpg"></a>
            <a href="img/pictures-large/80.jpg"> <img src="img/pictures-small/80-small.jpg"></a>
            <a href="img/pictures-large/81.jpg"> <img src="img/pictures-small/81-small.jpg"></a>
            <a href="img/pictures-large/82.jpg"> <img src="img/pictures-small/82-small.jpg"></a>
            <a href="img/pictures-large/83.jpg"> <img src="img/pictures-small/83-small.jpg"></a>
            <a href="img/pictures-large/84.jpg"> <img src="img/pictures-small/84-small.jpg"></a>
            <a href="img/pictures-large/85.jpg"> <img src="img/pictures-small/85-small.jpg"></a>
            <a href="img/pictures-large/86.jpg"> <img src="img/pictures-small/86-small.jpg"></a>
            <a href="img/pictures-large/87.jpg"> <img src="img/pictures-small/87-small.jpg"></a>
            <a href="img/pictures-large/88.jpg"> <img src="img/pictures-small/88-small.jpg"></a>
            <a href="img/pictures-large/89.jpg"> <img src="img/pictures-small/89-small.jpg"></a>
            <a href="img/pictures-large/90.jpg"> <img src="img/pictures-small/90-small.jpg"></a>
            <a href="img/pictures-large/91.jpg"> <img src="img/pictures-small/91-small.jpg"></a>
            <a href="img/pictures-large/92.jpg"> <img src="img/pictures-small/92-small.jpg"></a>
            <a href="img/pictures-large/93.jpg"> <img src="img/pictures-small/93-small.jpg"></a>
            <a href="img/pictures-large/94.jpg"> <img src="img/pictures-small/94-small.jpg"></a>
            <a href="img/pictures-large/95.jpg"> <img src="img/pictures-small/95-small.jpg"></a>
            <a href="img/pictures-large/96.jpg"> <img src="img/pictures-small/96-small.jpg"></a>
            <a href="img/pictures-large/97.jpg"> <img src="img/pictures-small/97-small.jpg"></a>
            <a href="img/pictures-large/98.jpg"> <img src="img/pictures-small/98-small.jpg"></a>
            <a href="img/pictures-large/99.jpg"> <img src="img/pictures-small/99-small.jpg"></a>
            <a href="img/pictures-large/100.jpg"> <img src="img/pictures-small/100-small.jpg"></a>

             
          </div>   
      </div>
      <br><br><br><br><br>
        <h6 class="text-center"><a href="gallery.php">Back</a></h6>
      </div>
    </section>

<?php include 'php/footer.php'?>

</body>
</html>