<?php include 'php/header.php' ?>



    <!-- Header -->
    <header class="masthead">
      <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
          <h1 class="mx-auto my-0 text-uppercase">studio 4104</h1>
          <h2 class="text-white-50 mx-auto mt-2 mb-5"></h2>
          <a href="#about" class="btn btn-primary js-scroll-trigger">Get Started</a>
        </div>
      </div>
    </header>

    <?php include 'php/projectrows.php'?>

    <?php include 'php/footer.php'?>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/grayscale.min.js"></script>

    

  </body>

</html>