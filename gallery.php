<?php include 'php/header2.php'?>

<section id="projects" class="projects-section bg-light">
    <div class="container">

        <div class="col-md-12 mb-9 mb-md-0">
          <div class="gallery">
            <a href="img/pictures-large/1.jpg" data-lightbox="mygallery"><img src="img/pictures-small/1-small.jpg"></a>
            <a href="img/pictures-large/2.jpg"> <img src="img/pictures-small/2-small.jpg"></a>
            <a href="img/pictures-large/3.jpg"> <img src="img/pictures-small/3-small.jpg"></a>
            <a href="img/pictures-large/4.jpg"> <img src="img/pictures-small/4-small.jpg"></a>
            <a href="img/pictures-large/5.jpg"> <img src="img/pictures-small/5-small.jpg"></a>
            <a href="img/pictures-large/6.jpg"> <img src="img/pictures-small/6-small.jpg"></a>
            <a href="img/pictures-large/7.jpg"> <img src="img/pictures-small/7-small.jpg"></a>
            <a href="img/pictures-large/8.jpg"> <img src="img/pictures-small/8-small.jpg"></a>
            <a href="img/pictures-large/9.jpg"> <img src="img/pictures-small/9-small.jpg"></a>
            <a href="img/pictures-large/10.jpg"> <img src="img/pictures-small/10-small.jpg"></a>
            <a href="img/pictures-large/11.jpg"> <img src="img/pictures-small/11-small.jpg"></a>
            <a href="img/pictures-large/12.jpg"> <img src="img/pictures-small/12-small.jpg"></a>
            <a href="img/pictures-large/13.jpg"> <img src="img/pictures-small/13-small.jpg"></a>
            <a href="img/pictures-large/14.jpg"> <img src="img/pictures-small/14-small.jpg"></a>
            <a href="img/pictures-large/15.jpg"> <img src="img/pictures-small/15-small.jpg"></a>
            <a href="img/pictures-large/16.jpg"> <img src="img/pictures-small/16-small.jpg"></a>
            <a href="img/pictures-large/17.jpg"> <img src="img/pictures-small/17-small.jpg"></a>
            <a href="img/pictures-large/18.jpg"> <img src="img/pictures-small/18-small.jpg"></a>
            <a href="img/pictures-large/19.jpg"> <img src="img/pictures-small/19-small.jpg"></a>
            <a href="img/pictures-large/20.jpg"> <img src="img/pictures-small/20-small.jpg"></a>
            <a href="img/pictures-large/21.jpg"> <img src="img/pictures-small/21-small.jpg"></a>
            <a href="img/pictures-large/22.jpg"> <img src="img/pictures-small/22-small.jpg"></a>
            <a href="img/pictures-large/23.jpg"> <img src="img/pictures-small/23-small.jpg"></a>
            <a href="img/pictures-large/24.jpg"> <img src="img/pictures-small/24-small.jpg"></a>
            <a href="img/pictures-large/25.jpg"> <img src="img/pictures-small/25-small.jpg"></a>
            <a href="img/pictures-large/26.jpg"> <img src="img/pictures-small/26-small.jpg"></a>
            <a href="img/pictures-large/27.jpg"> <img src="img/pictures-small/27-small.jpg"></a>
            <a href="img/pictures-large/28.jpg"> <img src="img/pictures-small/28-small.jpg"></a>
            <a href="img/pictures-large/29.jpg"> <img src="img/pictures-small/29-small.jpg"></a>
            <a href="img/pictures-large/30.jpg"> <img src="img/pictures-small/30-small.jpg"></a>
            <a href="img/pictures-large/31.jpg"> <img src="img/pictures-small/31-small.jpg"></a>
            <a href="img/pictures-large/32.jpg"> <img src="img/pictures-small/32-small.jpg"></a>
            <a href="img/pictures-large/33.jpg"> <img src="img/pictures-small/33-small.jpg"></a>
            <a href="img/pictures-large/34.jpg"> <img src="img/pictures-small/34-small.jpg"></a>
            <a href="img/pictures-large/35.jpg"> <img src="img/pictures-small/35-small.jpg"></a>
            <a href="img/pictures-large/36.jpg"> <img src="img/pictures-small/36-small.jpg"></a>
            <a href="img/pictures-large/37.jpg"> <img src="img/pictures-small/37-small.jpg"></a>
            <a href="img/pictures-large/38.jpg"> <img src="img/pictures-small/38-small.jpg"></a>
            <a href="img/pictures-large/39.jpg"> <img src="img/pictures-small/39-small.jpg"></a>
            <a href="img/pictures-large/40.jpg"> <img src="img/pictures-small/40-small.jpg"></a>
            <a href="img/pictures-large/41.jpg"> <img src="img/pictures-small/41-small.jpg"></a>
            <a href="img/pictures-large/42.jpg"> <img src="img/pictures-small/42-small.jpg"></a>
            <a href="img/pictures-large/43.jpg"> <img src="img/pictures-small/43-small.jpg"></a>
            <a href="img/pictures-large/44.jpg"> <img src="img/pictures-small/44-small.jpg"></a>
            <a href="img/pictures-large/45.jpg"> <img src="img/pictures-small/45-small.jpg"></a>
            <a href="img/pictures-large/46.jpg"> <img src="img/pictures-small/46-small.jpg"></a>
            <a href="img/pictures-large/47.jpg"> <img src="img/pictures-small/47-small.jpg"></a>
            <a href="img/pictures-large/48.jpg"> <img src="img/pictures-small/48-small.jpg"></a>
            <a href="img/pictures-large/49.jpg"> <img src="img/pictures-small/49-small.jpg"></a>
            <a href="img/pictures-large/50.jpg"> <img src="img/pictures-small/50-small.jpg"></a>

             
          </div>   
      </div>
      <br><br><br><br><br>
        <h6 class="text-center"><a href="gallery2.php">Next</a></h6>
      </div>
    </section>

<?php include 'php/footer.php'?>
</body>
</html>